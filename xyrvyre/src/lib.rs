//! # Xyrvyre
//!
//! Xyrvyre is a library for Nuvian on unix and perhaps windows

#[allow(unused_imports)]
use log::{debug, error, info};

use std::error::Error;

use async_tungstenite::tokio::client_async;
use async_tungstenite::tungstenite::protocol::Message;
use tokio::net::TcpStream;
use tokio::net::ToSocketAddrs;
use tokio::sync::mpsc;
#[allow(unused_imports)]
use tokio::sync::oneshot;
#[allow(unused_imports)]
use tokio::sync::Mutex;
#[allow(unused_imports)]
use tokio::task;

type Sender = mpsc::UnboundedSender<Message>;
type Receiver = mpsc::UnboundedReceiver<Message>;

pub async fn go(addr: impl ToSocketAddrs + std::fmt::Display) -> Result<(), Box<dyn Error>> {
    let stream = TcpStream::connect(&addr).await?;
    info!("Client connect: {addr}");

    let (ws_stream, ws_responce) = client_async("", stream)
        .await
        .expect("Error during WebSocket handshake");

    let (ws_sender, ws_receiver) = futures_util::StreamExt::split(ws_stream);

    // Create a channel for sending messages to this client
    let (client_tx, client_rx) = mpsc::unbounded_channel::<Message>();

    // Spawn a task to handle messages from the client
    tokio::spawn(handle_server_messages(client_rx, ws_sender, ws_responce));

    // Send messages from the server to the client
    tokio::spawn(send_messages_to_server(client_tx, ws_receiver));

    Ok(())
}

async fn handle_server_messages(
    mut rx: Receiver,
    mut ws_sender: impl futures_util::Sink<Message, Error = impl std::fmt::Debug> + std::marker::Unpin,
    _ws_responce: impl std::fmt::Debug,
) {
    while let Some(message) = rx.recv().await {
        use futures_util::SinkExt;
        ws_sender
            .send(message)
            .await
            .expect("Error sending message to client");
    }
}

async fn send_messages_to_server(
    tx: Sender,
    mut ws_receiver: impl futures_util::Stream<Item = Result<Message, async_tungstenite::tungstenite::Error>>
        + std::marker::Unpin,
) {
    use futures_util::StreamExt;
    while let Some(message) = ws_receiver.next().await {
        let message = message.expect("Error");
        tx.send(message).expect("Error sending message to clients");
    }
}

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
