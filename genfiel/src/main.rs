//! # Genfiel
//!
//! Genfiel is the reference implementation of a signaling server for Nuvian

#[allow(unused_imports)]
use log::{debug, error, info};

use ring::rand;
use ring::signature;

use std::error::Error;
use std::net::SocketAddr;
use std::sync::atomic::AtomicU32;
use std::sync::Arc;

use async_tungstenite::tokio::accept_async;
use async_tungstenite::tungstenite::protocol::Message;

use tokio::net::TcpListener;
use tokio::net::TcpStream;
use tokio::sync::mpsc;
#[allow(unused_imports)]
use tokio::sync::oneshot;
use tokio::sync::Mutex;
#[allow(unused_imports)]
use tokio::task;

pub mod nom_util;

type ClientId = u32;

fn generate_client_id() -> ClientId {
    // Generate or retrieve a unique numeric ID
    // For example, from a database sequence or other source.
    // For simplicity, this is just incrementing a counter in this example.
    static COUNTER: AtomicU32 = AtomicU32::new(0);
    COUNTER.fetch_add(1, std::sync::atomic::Ordering::SeqCst)
}

type Sender = mpsc::UnboundedSender<Message>;
type Receiver = mpsc::UnboundedReceiver<Message>;

struct Client {
    #[allow(dead_code)]
    sender: Sender,
    id: ClientId,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    let rng = Arc::new(ring::rand::SystemRandom::new());
    let h = keyring::Entry::new("nuvian_genfiel_1dc61808", "78d144144b0a6327")?;
    let secret: Arc<_> = ring::signature::Ed25519KeyPair::from_pkcs8(&base64::Engine::decode(
        &base64::engine::general_purpose::STANDARD_NO_PAD,
        h.get_password().or_else(|x| {
            if matches!(x, keyring::Error::NoEntry) {
                let password = generate_new_secret(rng.as_ref());
                h.set_password(&password)?;
                Ok(password)
            } else {
                Err(x)
            }
        })?,
    )?)
    .map_err(|_| "KeyRejected")?
    .into();

    let mut pkv = bevy_pkv::PkvStore::new("Nuvian", "Genfiel");

    let bind = pkv
        .get::<String>("bind")
        .or_else(|x| -> Result<_, Box<dyn std::error::Error>> {
            if matches!(x, bevy_pkv::GetError::NotFound) {
                let bind = "127.0.0.1:8080";
                pkv.set_string("bind", bind)?;
                Ok(bind.into())
            } else {
                Err(x)?
            }
        })?;
    let listener = TcpListener::bind(&bind).await.unwrap();
    info!("Server listening on: {bind}");

    let clients = Arc::new(Mutex::new(Vec::new()));

    let global = ConnectionGlobal {
        clients,
        secret,
        rng,
    };

    loop {
        let (stream, addr) = listener.accept().await?;
        tokio::spawn(handle_connection(ConnectionInfo {
            stream,
            addr,
            global: global.clone(),
        }));
    }
}

fn take<const C: usize>(input: &[u8]) -> nom::IResult<&[u8], &[u8; C]> {
    use nom::combinator::map_res;
    use nom::Parser;

    map_res(nom::bytes::complete::take(C), TryInto::try_into).parse(input)
}

#[derive(Clone)]
struct ConnectionGlobal {
    clients: Arc<Mutex<Vec<Client>>>,
    secret: Arc<signature::Ed25519KeyPair>,
    rng: Arc<rand::SystemRandom>,
}

struct ConnectionInfo {
    stream: TcpStream,
    addr: SocketAddr,
    global: ConnectionGlobal,
}

async fn handle_connection(
    ConnectionInfo {
        stream,
        addr,
        global:
            ConnectionGlobal {
                clients,
                secret,
                rng,
            },
    }: ConnectionInfo,
) -> Result<(), Box<dyn Error + 'static + Send + Sync>> {
    use futures_util::StreamExt;
    use nom::Parser;
    use signature::KeyPair;
    use std::time;

    debug!("Accepted connection from: {addr:?}");

    let iv: [u8; 32] = ring::rand::generate(rng.as_ref())
        .map_err(|_| "GenerateIV")?
        .expose();
    let time = time::SystemTime::now()
        .duration_since(time::UNIX_EPOCH)
        .expect("You must be far into the future, HI.")
        .as_secs()
        .to_be_bytes();
    let public: &[u8; 32] = secret.public_key().as_ref().try_into()?;

    let mut body = [iv.as_slice(), time.as_slice(), public.as_slice()].concat();
    let sig = secret.sign(body.as_slice());

    body.extend(sig.as_ref().iter());

    let ws_stream = accept_async(stream)
        .await
        .expect("Error during WebSocket handshake");

    let (ws_sender, mut ws_receiver) = futures_util::StreamExt::split(ws_stream);

    // Create a channel for sending messages to this client
    let (client_tx, client_rx) = mpsc::unbounded_channel::<Message>();
    let id = generate_client_id();
    // Spawn a task to handle messages to the client
    tokio::spawn(handle_client_outbound(
        clients.clone(),
        addr,
        id,
        client_rx,
        ws_sender,
    ));

    let outgoing_hello = Message::Binary(body);
    client_tx.send(outgoing_hello)?;

    let input = match ws_receiver.next().await.ok_or("bye")?? {
        Message::Binary(x) => Ok(x),
        _ => Err("NotBinary"),
    }?;

    use nom::multi::many1;
    use nom::number::complete::{be_i16, be_u64};
    use nom::Finish;
    let (_, ((message, (_iv, _client_time, client_public, _behaviours)), signature)) =
        nom::combinator::complete(
            nom::combinator::consumed(nom::sequence::tuple((
                take::<32>,
                be_u64,
                take::<32>,
                many1(be_i16.and(nom_util::null_str)),
            )))
            .and(take::<64>),
        )
        .parse(input.as_slice())
        .finish()
        .map_err(|_| "ClientHelloParser")?;

    let client_public =
        signature::UnparsedPublicKey::new(&signature::EdDSAParameters, client_public);

    client_public
        .verify(message, signature)
        .map_err(|_| "FailedEdDSAVerify")?;

    let client = Client {
        sender: client_tx,
        id,
    };

    // Add the client to the list of connected clients
    clients.as_ref().lock().await.push(client);

    Ok(())
}

async fn handle_client_outbound(
    clients: impl AsRef<Mutex<Vec<Client>>>,
    addr: SocketAddr,
    id: ClientId,
    mut rx: Receiver,
    mut ws_sender: impl futures_util::Sink<Message, Error = impl std::fmt::Debug> + std::marker::Unpin,
) {
    while let Some(message) = rx.recv().await {
        use futures_util::SinkExt;
        ws_sender
            .send(message)
            .await
            .expect("Error sending message to client");
    }

    // Remove the client from the list of connected clients
    let mut clients = clients.as_ref().lock().await;
    clients.retain(|c| c.id != id);
    debug!("Client {addr:?} disconnected");
}

fn generate_new_secret(rng: &dyn ring::rand::SecureRandom) -> String {
    let input = ring::signature::Ed25519KeyPair::generate_pkcs8(rng).unwrap();
    base64::Engine::encode(&base64::engine::general_purpose::STANDARD_NO_PAD, input)
}
