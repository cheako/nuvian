use nom::bytes::complete::take_till;
use nom::combinator::{into, map, map_res};
use nom::sequence::terminated;
use nom::IResult;

pub fn byte(t: u8) -> impl Fn(&[u8]) -> IResult<&[u8], ()> {
    move |i: &[u8]| {
        if i[0] == t {
            Ok((&i[1..], ()))
        } else {
            nom::combinator::fail(i)
        }
    }
}

/// Bytes ending in null into an owned.
pub fn null_vec(input: &[u8]) -> IResult<&[u8], Vec<u8>> {
    map(null_bytes, Into::into)(input)
}

/// An array ending in a null.
pub fn null_bytes(input: &[u8]) -> IResult<&[u8], &[u8]> {
    terminated(take_till(|x| x == 0), byte(0))(input)
}

/// An array /w null into an owned utf8
pub fn null_string(input: &[u8]) -> IResult<&[u8], String> {
    map(null_str, Into::into)(input)
}

/// An array /w null as utf8 &str
pub fn null_str(input: &[u8]) -> IResult<&[u8], &str> {
    map_res(into(null_bytes), std::str::from_utf8)(input)
}
